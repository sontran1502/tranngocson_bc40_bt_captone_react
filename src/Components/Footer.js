import React from 'react';
import "./Footer.css";
import zion from "../asset/zion.jpg";
import dtb from "../asset/dathongbao.png"
export default function Footer() {
  return (
    <div className='footer text-white pb-6'>
      <div  className='container flex mt-6 footer__content'>
      <div className="footer__img">
        <img src={`${zion}`} alt="" />
      </div>
      <div className="flex-grow ">
        <h5 className='center__title'>
          TIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION
        </h5>
        <h5>
          Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí Minh, Việt Nam.
        </h5>
        <h5>
          Giấy chứng nhận đăng ký kinh doanh số: 0101659783,
        </h5>
        <h5>
          đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020 do Sở kế hoạch và đầu tư Thành phố Hồ Chí Minh cấp.
        </h5>
        <h5>
          Số Điện Thoại (Hotline): 1900 545 436
        </h5>

      </div>
      <div className="footer__img">
      <img src={`${dtb}`} alt="" />
      </div>
    </div>
    </div>
  )
}
