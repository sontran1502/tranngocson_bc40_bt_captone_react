import React from 'react'
import { useSelector } from 'react-redux'
import { RingLoader } from 'react-spinners'

export default function Spinner() {
    let {isLoading}= useSelector((state)=>state.spinnerSlice)
  return isLoading ? (
    <div className='h-screen w-screen fixed top-0 right-0 flex justify-center items-center z-50'
        style={{background:"#f2e9e4"}}
    >
        <RingLoader color="#00b4d8"  size={100}/>
    </div>
  ):
  <></>
}
