
import React from 'react'
import { localUserServ } from '../Services/localUserServ'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import  './MenuLogin.css';

export default function MenuLogin() {
    let userInfo = useSelector((state) => {
        return state.userSlice.userInfo
    })

    let handleLogout = () => {
        localUserServ.remote();
        window.location.reload()
    }

    let renderContent = () => {
        
        if (userInfo) {
            return (
                <>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    <span className='mx-2'>{userInfo.hoTen}</span>

                    
                        <button onClick={handleLogout} className='custom-btn btn-15'>
                            Đăng Xuất
                        </button>
                    

                </>)
        } else {
            return (<>
                <NavLink to={"/login"}>
                    <button className='custom-btn btn-15 mr-3'>
                        Đăng nhập
                    </button>
                </NavLink>
                <NavLink to={"/register"}>
                    <button className='custom-btn btn-15'>
                        Đăng Ký
                    </button>
                </NavLink>
            </>)


        }
    }
    return (
        <div className=' flex justify-center items-center text-black'>
            {renderContent()}
        </div>
    )
}
