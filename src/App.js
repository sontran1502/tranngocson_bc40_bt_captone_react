import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './Pages/HomePage/HomePage';
import Layout from './Layout/Layout';
import LoginPage from './Pages/LoginPage/LoginPage';
import Register from './Pages/Register/Register';
import DetailPage from './Pages/DetailPage/DetailPage';
import Spinner from './Components/Spinner/Spinner';
import PageNotFound from './Pages/PageNotFound/PageNotFound';

function App() {
  return (
    <div >
      <Spinner/>
     <BrowserRouter>
     <Routes>
      <Route path='/' element={<HomePage/>}/>
      <Route path='/login' element={<Layout Component={LoginPage}/>}/>
      <Route path='/register' element={<Layout Component={Register}/>}/>
      <Route path='/detail/:id' element={<Layout Component={DetailPage}/>}/>
      <Route path='*' element={<PageNotFound/>}/>
     </Routes>
     </BrowserRouter>
    </div>
  );
}

export default App;
