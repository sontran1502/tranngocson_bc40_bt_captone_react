import React from 'react'
import Header from '../Components/Header'
import bg from "../asset/bg.jpg";


export default function ({ Component }) {
  return (
    <div 
    className='min-h-screen h-full flex flex-col space-y-10'
    >
      <Header />
      <div className='h-screen w-full flex justify-center items-center 	'
      style={{
        backgroundImage:`url(${bg})`,
        backgroundSize:"cover",
        backgroundPosition:"center"
      }}
      
       >
        <Component />
      </div>

    </div>
  )
}
