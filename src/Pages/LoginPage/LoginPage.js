import React, { useState } from 'react'
import {  message } from 'antd';
import { NavLink, useNavigate } from 'react-router-dom';
import { userServ } from '../../Services/userServ';
import { localUserServ } from '../../Services/localUserServ';
import { useDispatch } from 'react-redux';
import { setUserLogin } from '../../toolkit/userSlice';


const LoginPage = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate()
  let [formLogin, setFormLogin] = useState({ taiKhoan: "", matKhau: "" })
    let handleSubmit = e => {
        e.preventDefault()
        console.log("formLogin", formLogin)
        userServ
            .postLogin(formLogin)
            .then((res) => {
              message.success("Đăng nhập thành công")
              localUserServ.set(res.data.content)
              navigate("/")
              dispatch(setUserLogin(res.data.content))
            })
            .catch((err) => {
              console.log(err)
                message.error(err.response.data.content);
            });
    }
  return (
    
    <div className="flex items-center w-96 antialiased text-gray-900 font-sans">
      <div className="w-full bg-white rounded shadow-lg p-8 m-4 md:max-w-2xl md:mx-auto">
        <span className="block w-full text-3xl uppercase font-bold mb-4">Đăng Nhập</span>
        <form className="mb-4" method="post" onSubmit={handleSubmit}>
          <div className="mb-4 md:w-full">
            <label htmlFor="taiKhoan" className="block text-lg mb-1">
              Tài Khoản
            </label>
            <input className="w-full border rounded p-2 outline-none focus:shadow-outline"
              type="text" id="taiKhoan" placeholder="Nhập Tài Khoản"
              name="taiKhoan"
              value={formLogin.taiKhoan}
              onChange={e => setFormLogin({ ...formLogin, taiKhoan: e.target.value })} />
          </div>
          <div className="mb-6 md:w-full">
            <label htmlFor="matKhau" className="block text-lg mb-1">Mật Khẩu</label>
            <input className="w-full border rounded p-2 outline-none focus:shadow-outline" type="password" id="matKhau" placeholder="Mật Khẩu"
              name="matKhau"
              value={formLogin.matKhau}
              onChange={e => setFormLogin({ ...formLogin, matKhau: e.target.value })}
            />
          </div>
          <button className="bg-green-500 hover:bg-green-700 text-white uppercase text-sm font-semibold px-4 py-2 rounded" type='submit'>
            Đăng Nhập
          </button>
        </form>
        <h5>
          Bạn Chưa Có Tài Khoản?
          <NavLink to={"/register"} className="text-blue-700 text-center ml-2 text-ms" href="/login">
            Đăng Ký
          </NavLink>
        </h5>
      </div>
    </div>

  )
}
export default LoginPage;
