import React, { useEffect, useState } from 'react'
import { movieServ } from '../../../Services/movieServ'
import { Tabs } from 'antd';
import ItemTabMovie from './ItemTabMovie';

const onChange = (key) => {
    console.log(key);
};
export default function TabMovie() {
    const [heThong, setHeThong] = useState([])
    useEffect(() => {
        movieServ
            .getMovieByTheater()
            .then((res) => {
                console.log(res);
                setHeThong(res.data.content)
            })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    let renderHeThongRap = () => {
        return heThong.map((rap) => {
            return {
                key: rap.maHeThongRap,
                label: <img className='h-16' src={rap.logo} alt="" />,
                children:
                    <Tabs
                        className=''
                        style={{   height: 620, }}
                        tabPosition="left"
                        defaultActiveKey="1"
                        items={rap.lstCumRap.slice(0,10).map((cumRap) => {
                            return {
                                key: cumRap.tenCumRap,
                                label: <div>{cumRap.tenCumRap}</div>,
                                children:(
                                    <div
                                    style={{ height: 620,
                                            
                                    }}
                                    className='overflow-y-scroll' >
                                        {cumRap.danhSachPhim.map((item) => { 
                                            return <div><ItemTabMovie phim={item}/></div>
                                         })}
                                    </div>
                                )

                            }
                        })}
                        onChange={onChange}
                    />
            }
        })
    }
    return (
        <div>
            <div 
            style={{
                marginTop:"35px"
            }}
            className='container '>
                {" "}
                <Tabs
                    className='border border-zinc-300'
                    style={{  height: 620, }}
                    tabPosition="left"
                    defaultActiveKey="1"
                    items={renderHeThongRap()}
                    onChange={onChange} />
            </div>
        </div>
    )
}
