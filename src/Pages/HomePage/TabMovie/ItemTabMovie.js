import React from 'react'
import moment from 'moment/moment'


export default function ItemTabMovie({phim}) {
  return (
    <div className='p-5 my-5 flex'>
        <img className='w-36 h-36' src={phim.hinhAnh} alt="" />
        <div className='mx-5'>
            <h3 className='font-medium text-xl'>{phim.tenPhim}</h3>
            <div className="my-3 grid grid-cols-3 gap-5">
            {phim.lstLichChieuTheoPhim.slice(0,9).map((item) => { 
                    return(
                        <span className=' rounded p-2 bg-blue-500 text-white font-medium'>
                            {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY ~ hh:mm")}
                        </span>
                    )
                 })}
            </div>
        </div>

    </div>
  )
}
