import React from 'react'
import Header from '../../Components/Header'
import Banner from './Banner/Banner'
import ListMovie from './ListMovie/ListMovie'
import TabMovie from './TabMovie/TabMovie'
import Program from './Program/Program'
import Footer from '../../Components/Footer'

export default function HomePage() {
  return (
    <div className='relative'>
      <Header/>
      <Banner/>
      <ListMovie/>
      <TabMovie/>
      <Program/>
      <Footer/>
     
    </div>
  )
}
