import React from 'react'
import { Card } from 'antd';
import { NavLink } from 'react-router-dom';
const { Meta } = Card;

export default function ItemMovie({ movie }) {
    return (
        <div  style={{
            width: "240px",
            
        }}>
            <Card
                hoverable
                
                cover={<img 
                    style={{
                        width: "240px",
                        height:"240px",
                    }}
                    className=' object-cover object-top'
                    alt="example" 
                    src={movie.hinhAnh} />}
            >
                <Meta title={movie.tenPhim} 
                description={<NavLink to={`/detail/${movie.maPhim}`}>
                    <button>Mua Vé</button>
                </NavLink>} />
            </Card>
        </div>
    )
}
