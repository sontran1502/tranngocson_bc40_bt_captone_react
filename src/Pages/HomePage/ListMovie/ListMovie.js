import React, { useEffect, useState } from 'react'
import { movieServ } from '../../../Services/movieServ'
import ItemMovie from './ItemMovie';
import { useDispatch } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../../toolkit/spinnerSlice';

export default function ListMovie() {

  const [movie, setMovie] = useState([])
  let dispatch = useDispatch()
    useEffect(() => { 
      dispatch(setLoadingOn())
        movieServ
        .getMovieList()
        .then((res) => {
                dispatch(setLoadingOff())
                setMovie(res.data.content)
              })
              .catch((err) => {
                dispatch(setLoadingOff())
              });
     },[]);
  return (
    <div 
     className='container '>
      <div className="my-6 grid grid-cols-4 gap-5">
      {movie.map((item) => { 
        return <ItemMovie movie={item} key={item.maPhim} />
       })}
      </div>
    </div>
  )
}
