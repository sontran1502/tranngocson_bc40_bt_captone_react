import React, { useEffect, useState } from 'react'
import { Carousel } from 'antd';
import { bannerServ } from '../../../Services/bannerServ';
import { useDispatch } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../../toolkit/spinnerSlice';

const contentStyle = {
    height: '80vh',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',

};
const Banner = () => {
    const [arrImg, setArrImg] = useState([])
    let dispatch = useDispatch()
    useEffect(() => {
        dispatch(setLoadingOn())
        bannerServ
            .getBanner()
            .then((res) => {
                dispatch(setLoadingOff())
                setArrImg(res.data.content);
            })
            .catch((err) => {
                dispatch(setLoadingOff())
            });
    }, []);

    let renderContent = () => {
        return arrImg.map((item, index) => {
            return (
                <div key={index}>
                    <div>
                        <img
                            className='w-full'
                            style={contentStyle}
                            src={item.hinhAnh}
                            alt="" />
                    </div>
                </div>
            )


        })
    }
    return (
        <div style={{
            marginTop: "35px"
        }}>
            <Carousel autoplay>
                {renderContent()}
            </Carousel>
        </div>
    )
};
export default Banner;
