import React, { useState } from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import "./register.css"
import { userServ } from '../../Services/userServ'
import { message } from 'antd'
import { localUserServ } from '../../Services/localUserServ'

export default function RegisterPage() {
  let navigate = useNavigate()
    let [formRegister, setFormRegister] = useState({
        taiKhoan: "",
        matKhau: "",
        email: "",
        soDt: "",
        maNhom: "",
        hoTen: "",
        confirmPass: ""
    })

    let [focused,setFocused]= useState(false)

    let handleFocuse =(e)=>{
        setFocused(true)
    }
    let handleSubmit = e => {
        e.preventDefault()
        console.log("formRegister", formRegister)

        userServ
            .postRegister(formRegister)
            .then((res) => {
                message.success("Đăng ký thành công")
                navigate("/login")
            })
            .catch((err) => {
              message.error("Đăng ký thất bại")

            });
    }
    return (
       
            <div className="flex items-center w-96 antialiased text-gray-900 font-sans">
                <div className="w-full bg-white rounded shadow-lg p-8 m-4 ">
                    <span className="block w-full text-3xl uppercase font-bold mb-4">Đăng Ký</span>
                    <form className="mb-4" method="post" onSubmit={handleSubmit}>
                    <div className="mb-4 md:w-full">
                            {/* <label htmlFor="hoTen" className="block text-lg mb-1">
                                Họ Và Tên
                            </label> */}
                            <input className="w-full border rounded p-2 outline-none "
                                type="text" id="hoTen" placeholder="Nhập Họ Và Tên"
                                name="hoTen"
                                value={formRegister.hoTen}
                                onChange={e => setFormRegister({ ...formRegister, hoTen: e.target.value })} 
                                // onBlur={handleFocuse} focused={focused.toString()}
                                
                                required/>
                             {/* <span className='note'>Đây là trường bắt buộc !</span> */}

                        </div>
                        <div className="mb-4 md:w-full">
                            {/* <label htmlFor="taiKhoan" className="block text-lg mb-1">
                                Tài Khoản
                            </label> */}
                            <input className="w-full border rounded p-2 outline-none "
                                type="text" id="taiKhoan" placeholder="Nhập Tài Khoản"
                                name="taiKhoan"
                                value={formRegister.taiKhoan}
                                onChange={e => setFormRegister({ ...formRegister, taiKhoan: e.target.value })}
                                // onBlur={handleFocuse} focused={focused.toString()}
                                required/>
                             {/* <span className='note'>Đây là trường bắt buộc !</span> */}
                        </div>
                        <div className="mb-6 md:w-full">
                            {/* <label htmlFor="matKhau" className="block text-lg mb-1">Mật Khẩu</label> */}
                            <input className="w-full border rounded p-2 outline-none " type="password" id="matKhau" placeholder="Mật Khẩu"
                                name="matKhau"
                                value={formRegister.matKhau}
                                pattern='^.{6,}$'
                                onChange={e => setFormRegister({ ...formRegister, matKhau: e.target.value })}
                                onBlur={handleFocuse} focused={focused.toString()}
                                required
                                />
                             <span className='note'>Mật khẩu phải có ít nhất 6 ký tự!</span>

                        </div>
                        <div className="mb-6 md:w-full">
                            {/* <label htmlFor="confirmPass" className="block text-lg mb-1">Nhập lại Mật Khẩu Mật Khẩu</label> */}
                            <input className="w-full border rounded p-2 outline-none " type="password" id="confirmPass" placeholder="Nhập Lại Mật Khẩu"
                                name="confirmPass"
                                value={formRegister.confirmPass}
                                pattern={formRegister.matKhau}
                                onChange={e => setFormRegister({ ...formRegister, confirmPass: e.target.value })}
                                onBlur={handleFocuse} focused={focused.toString()}
                                required
                                />
                             <span className='note'>Mật khẩu không khớp lệ!</span>

                        </div>
                        
                        <div className="mb-4 md:w-full">
                            {/* <label htmlFor="email" className="block text-lg mb-1">
                                Email
                            </label> */}
                            <input className="w-full border rounded p-2 outline-none "
                                type="email" id="email" placeholder="Nhập Email"
                                name="email"
                                value={formRegister.email}
                                pattern='/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i'
                                onChange={e => setFormRegister({ ...formRegister, email: e.target.value })} 
                                onBlur={handleFocuse} focused={focused.toString()}
                                
                                 required
                                />
                             <span className='note'>Email không hợp lệ!</span>
                        </div>
                        <button className="bg-green-500 hover:bg-green-700 text-white uppercase text-sm font-semibold px-4 py-2 rounded" >
                            Đăng Ký
                        </button>
                    </form>
                    <h5>
                        Bạn đã Có Tài Khoản?
                        <NavLink to={"/login"} className="text-blue-700 text-center ml-2 text-ms" href="/login">
                            Đăng Nhập
                        </NavLink>
                    </h5>
                </div>
            </div>
      
    )
}